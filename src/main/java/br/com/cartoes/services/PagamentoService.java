package br.com.cartoes.services;

import br.com.cartoes.DTOs.RealizarPagamentoDTO;
import br.com.cartoes.DTOs.RealizarPagamentoResponseDTO;
import br.com.cartoes.models.Cartao;
import br.com.cartoes.models.Pagamento;
import br.com.cartoes.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;
    @Autowired
    private CartaoService cartaoService;

    public RealizarPagamentoResponseDTO salvarPagamento(RealizarPagamentoDTO realizarPagamentoDTO){
        Cartao cartao = cartaoService.buscarCartaoPorId(realizarPagamentoDTO.getCartao_id());
        Pagamento pagamento = realizarPagamentoDTO.converterDTOParaPagamento(cartao);
        pagamentoRepository.save(pagamento);
        RealizarPagamentoResponseDTO realizarPagamentoResponseDTO = new RealizarPagamentoResponseDTO(cartao.getId(),
                pagamento.getDescricao(), pagamento .getValor());
        return realizarPagamentoResponseDTO;
    }

}

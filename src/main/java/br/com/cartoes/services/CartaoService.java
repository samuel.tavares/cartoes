package br.com.cartoes.services;

import br.com.cartoes.DTOs.AtivarDesativarCartaoDTO;
import br.com.cartoes.models.Cartao;
import br.com.cartoes.models.Cliente;
import br.com.cartoes.repositories.CartaoRepository;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.Entity;
import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;
    @Autowired ClienteService clienteService;

    public Cartao salvarCartao(Cartao cartao){

        Cliente clienteDB = clienteService.buscarClientePorNome(cartao.getCliente().getNome());
        cartao.setCliente(clienteDB);
        return cartaoRepository.save(cartao);
    }

    public Cartao buscarCartaoPorId(int id){
        Optional<Cartao> cartao = cartaoRepository.findById(id);
        if (!cartao.isPresent()){
            throw new RuntimeException("Cartão não encontrado");
        }
        return cartao.get();
    }

    public Cartao ativarDesativarCartao(int id, AtivarDesativarCartaoDTO ativarDesativarCartaoDTO) {
        Optional<Cartao> cartaoDB = cartaoRepository.findById(id);
        if (!cartaoDB.isPresent()) {
            throw new RuntimeException("Cartão não encontrado");
        }
        Cartao cartao = cartaoDB.get();

        //caso ativar
        if (ativarDesativarCartaoDTO.isAtivo()) {
            if (cartaoDB.get().isAtivo()) {
                throw new RuntimeException("Cartão já ativado");
            }
            cartao.setAtivo(true);
            return cartaoRepository.save(cartao);
        }

        //caso inativar
        if (!cartaoDB.get().isAtivo()) {
            throw new RuntimeException("Cartão já inativo");
        }
        cartao.setAtivo(false);
        return cartaoRepository.save(cartao);
    }
}

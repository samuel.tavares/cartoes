package br.com.cartoes.services;

import br.com.cartoes.models.Cliente;
import br.com.cartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente salvarCliente(Cliente cliente){
        return clienteRepository.save(cliente);
    }

    public Cliente buscarClientePorId(int id){
        Optional<Cliente> cliente = clienteRepository.findById(id);
        if(cliente.isPresent()){
            return cliente.get();
        }else{
            throw new RuntimeException("Cliente não encontrado");
        }
    }

    public Cliente buscarClientePorNome(String nome){
        Optional<Cliente> cliente = Optional.ofNullable(clienteRepository.findByNome(nome));
        if (!cliente.isPresent()){
            throw new RuntimeException("Cliente não encontrado");
        }
        return cliente.get();
    }

}

package br.com.cartoes.controllers;

import br.com.cartoes.DTOs.RealizarPagamentoDTO;
import br.com.cartoes.DTOs.RealizarPagamentoResponseDTO;
import br.com.cartoes.models.Pagamento;
import br.com.cartoes.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/pagamento")
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @PostMapping
    public RealizarPagamentoResponseDTO salvarPAgamento(@RequestBody @Valid RealizarPagamentoDTO realizarPagamentoDTO){
        try {
            return pagamentoService.salvarPagamento(realizarPagamentoDTO);
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}

package br.com.cartoes.controllers;

import br.com.cartoes.DTOs.AtivarDesativarCartaoDTO;
import br.com.cartoes.models.Cartao;
import br.com.cartoes.services.CartaoService;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cartao salvarCartao(@RequestBody @Valid Cartao cartao){
        try {
            return cartaoService.salvarCartao(cartao);
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PatchMapping("/{id}")
    public Cartao ativarCartao(@RequestBody AtivarDesativarCartaoDTO ativarDesativarCartaoDTO, @PathVariable(name = "id")int id){
        try{
            return cartaoService.ativarDesativarCartao(id, ativarDesativarCartaoDTO);
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Cartao buscarCartaoPorId(@PathVariable(name = "id")int id){
        try{
            return cartaoService.buscarCartaoPorId(id);
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}

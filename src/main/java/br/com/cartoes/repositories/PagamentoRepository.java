package br.com.cartoes.repositories;

import br.com.cartoes.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {
}

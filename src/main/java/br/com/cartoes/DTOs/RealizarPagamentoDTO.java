package br.com.cartoes.DTOs;

import br.com.cartoes.models.Cartao;
import br.com.cartoes.models.Pagamento;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class RealizarPagamentoDTO {

    private int cartao_id;

    @NotNull(message = "Descrição não pode ser nulo")
    @NotBlank(message = "Descrição não pode ser vazio")
    private String descricao;

    @DecimalMin(value = "1.00")
    private double valor;

    public RealizarPagamentoDTO() {
    }

    public int getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(int cartao_id) {
        this.cartao_id = cartao_id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descrcao) {
        this.descricao = descrcao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public Pagamento converterDTOParaPagamento(Cartao cartao){
        Pagamento pagamento = new Pagamento();
        pagamento.setCartao(cartao);
        pagamento.setDescricao(this.descricao);
        pagamento.setValor(this.valor);
        return pagamento;
    }
}

package br.com.cartoes.DTOs;

public class AtivarDesativarCartaoDTO {
    private boolean ativo;

    public AtivarDesativarCartaoDTO() {
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
